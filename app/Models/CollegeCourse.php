<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class CollegeCourse extends Authenticatable
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'college_id','course_id','seat_no','reserved_seat','merit_seat'
    ];

    public function College()
    {
        return $this->belongsTo(College::class,'college_id','id');
    }

    public function Course()
    {
        return $this->belongsTo(Course::class,'course_id','id');
    }
}
