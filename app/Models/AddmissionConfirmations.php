<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AddmissionConfirmations extends Model
{
    use HasFactory;

    protected $fillable = [
        'addmission_id','confirm_college_id','confirm_round_id','confirm_merit','confirmation_type'
    ];

    public function college()
    {
        return $this->belongsTo(College::class,'confirm_college_id','id');
    }
}
