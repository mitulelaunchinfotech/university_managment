<?php

namespace App\Http\Requests\Student\Register;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'          =>'required',
            'email'         =>'required|email|unique:users',
            'contact_no'    =>'required|numeric|digits_between:9,11',
            'password'      =>'required',
            'address'       =>'required',
            'adhaar_card_no'=>'required',
            'image'         =>'required',
            'dob'           =>'required|date'
        ];
    }
}
