<?php

namespace App\Http\Requests\CollageCourse;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'course_id'=>'exists:college_courses,course_id',
            'reserved_seat'=>'required',
            'merit_seat'=>'required'
        ];
    }
}
