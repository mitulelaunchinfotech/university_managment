<?php

namespace App\Http\Requests\Collage\Register;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required',
            'email'=>'required|email|unique:colleges',
            
            'password'=>'required',
            'address'=>'required',
            'logo'=>'required|mimes:jpeg,png,jpg'
        ];
    }
}
