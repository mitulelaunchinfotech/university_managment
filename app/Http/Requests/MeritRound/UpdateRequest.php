<?php

namespace App\Http\Requests\MeritRound;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'round_no' => 'required',
            'fromDate'=>'required|date_format:Y-m-d',
            'toDate'=>'required|date',
            'resultdate'=>'required|date',
            'course_id' => 'required',
            'status'=>'exists:merit_rounds,status'
        ];
    }

    public function messages()
    {
        return [
            'fromdate.required'=>'Please select start date',
            'todate.required'=>'Please select end date',
            'resultdate.required'=>'Please result  date',
            'course_id' => 'required',
            'status'=>'required'
        ];
    }
}
