<?php

namespace App\Http\Requests\MeritRound;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'round_no' => 'required',
            'course_id'=>'exists:merit_rounds,course_id',
            'fromdate'=>'required|date_format:Y-m-d',
            'todate'=>'required|date',
            'resultdate'=>'required|date',
            'course_id' => 'required',
            'status'=>'exists:merit_rounds,status'
        ];
    }

    public function messages()
    {
        return [
            'round_no' => 'required',
            'fromdate.required'=>'Please select start date',
            'todate.required'=>'Please select end date',
            'resultdate.required'=>'Please result  date',
            'course_id' => 'required',
            'status'=>'required'
        ];
    }
}
