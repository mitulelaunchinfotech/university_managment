<?php

namespace App\Http\Requests\Admission;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'course_id'=>'required',
            'college_id'=>'required',
            'addmission_date'=>'required',              
        ];
    }

    public function messages()
    {
        return [
            'course_id.required'=>'Please select course',
            'addmission_date.required'=>'Please select date'
        ];
    }
}
