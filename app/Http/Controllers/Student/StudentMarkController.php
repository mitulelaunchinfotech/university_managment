<?php

namespace App\Http\Controllers\Student;

use App\DataTables\StudentMarkDataTable;
use App\Http\Controllers\Controller;
use App\Http\Requests\StudentMark\StoreRequest;
use App\Models\StudentMark;
use App\Models\Subject;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class StudentMarkController extends Controller
{
    public function index(StudentMarkDataTable $studentmarkDataTable)
    {
        return  $studentmarkDataTable->render('student.mark.index');
    }

    public function create()
    {
        $subject = Subject::select('id', 'name')->get();
        return view('student.mark.create', compact('subject'));
    }

    public function store(StoreRequest $request)
    {
        $user_id = request()->user()->id;
        foreach ($request['subject_id'] as $key => $data) {
            StudentMark::create([
                'user_id' => $user_id,
                'subject_id' => $key,
                'total_mark' => $request->total_mark,
                'obtain_mark' => $data
            ]);
        }
        return response()->json('1');
    }

    public function edit(Request $id)
    {
        $subject = Subject::findOrFail($id);
        $student_mark = StudentMark::with('Subject')->where('user_id', Auth::guard('student')->user()->id)->get();
        return view('student.mark.edit', compact('student_mark'));
    }

    public function update(Request $request)
    {
        dd($request->all());
        $student_mark = StudentMark::find($request->id);
        $student_mark->subject_id = $request->subject_id;
        $student_mark->save();
        return response()->json(['property' => $student_mark]);
    }
}


