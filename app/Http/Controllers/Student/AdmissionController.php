<?php

namespace App\Http\Controllers\Student;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admission\StoreRequest;
use App\Models\Addmission;
use App\Models\College;
use App\Models\Course;
use App\Models\MeritRound;
use App\Models\StudentMark;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;

class AdmissionController extends Controller
{
    public function create()
    {
        $course = Course::get();
        $collage = College::get();
        $merit_round = MeritRound::get();
        $dateYmd = date('Y-m-d');
        $form = MeritRound::where('start_date','<=',$dateYmd)->where('end_date','>=',$dateYmd)->first();
        return view('student.admission.create',compact('course','merit_round','collage','form'));
    }

    public function store(StoreRequest $request)
    {
        $studentmark = StudentMark::with('commonsetting')->where('id', Auth::guard('student')->user()->id)->get();
        $merit = 0;
        $com_total = 0;
        foreach ($studentmark as $studentmarks) {
            $total = ($studentmarks->obtain_mark * $studentmarks->commonsetting['marks'] ?? 0) / 100;
            $merit = $merit + $total;
            $com_total = $com_total + $studentmarks->commonsetting['marks'];
        }
        $f_merit = $merit / $com_total * 100;

        $student = StudentMark::where('id',Auth()->user()->id)->get();                   
        $course = Course::where('id',$request->course_id)->pluck('name')->first();  
        $addmission_code  = getRandomString($length = 6);
        $admission = new Addmission;
        $admission->user_id = request()->user()->id;
        $admission->merit = $f_merit;
        $admission->college_id = $request->college_id;
        $admission->addmission_date = $request->addmission_date;
        $admission->course_id = $request->course_id;    
        $admission->addmission_code = strtoupper($course) . $addmission_code;
        $admission->merit_round_id = $request->merit_round_no;
        $admission->status = 3;
        $admission->save();
        return response()->json('1');
    }  
    
    public function index(Request $request)
    {        
        $admission = Addmission::where('user_id',Auth()->user()->id)->first();                      
        $status = Addmission::where('user_id',Auth()->user()->id)->where('status',0)->first();               
        $status1 = Addmission::where('user_id',Auth()->user()->id)->where('status',2)->first();               
        return view('student.admission.index',compact('admission','status','status1'));
    }    
}
?>

