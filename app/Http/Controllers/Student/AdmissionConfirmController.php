<?php

namespace App\Http\Controllers\Student;

use App\Http\Controllers\Controller;
use App\Models\Addmission;
use App\Models\AddmissionConfirmations;
use App\Models\MeritRound;
use App\Repository\AdmissionConfirmeRepository;
use Illuminate\Http\Request;

class AdmissionConfirmController extends Controller
{
    public function __construct(AdmissionConfirmeRepository $admissionconfirmRepository)
    {
        $this->admissionconfirmRepository = $admissionconfirmRepository;
    }

    public function store(Request $request)
    {
        $store = $this->admissionconfirmRepository->store($request);
        return response()->json($store); 
    }

    public function changeStatus(Request $request)
    {       
        $user = Addmission::find($request->user_id);
        $user->status = $request->data_status;
        if ($request->data_status == 2) {
            $message = 'Rejected';
        } else if ($request->data_status == 0) {
            $message = 'You will appear next round';
        } else if ($request->data_status == 1) {
            $message = 'You will be selected';
        }else {
            $message = '';
        }
        $user->save();
        return response()->json(['message' => $message]);
    }
}
