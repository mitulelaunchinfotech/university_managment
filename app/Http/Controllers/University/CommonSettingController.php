<?php

namespace App\Http\Controllers\University;

use App\DataTables\CommonSettingDataTable;
use App\Http\Controllers\Controller;
use App\Http\Requests\CommonSetting\StoreRequest;
use App\Http\Requests\CommonSetting\UpdateRequest;
use App\Models\CommonSetting;
use App\Models\Subject;
use App\Repository\CommonSettingRepository;
use Illuminate\Http\Request;

class CommonSettingController extends Controller
{

    public function __construct(CommonSettingRepository $commonsettingRepository)
    {
        $this->commonsettingRepository = $commonsettingRepository;
    }

    public function index(CommonSettingDataTable $commonsettingDataTable)
    {
        return  $commonsettingDataTable->render('admin.common_setting.index');
    }

    public function create()
    {
        $subject = Subject::select('id', 'name')->get();
        return view('admin.common_setting.create', compact('subject'));
    }
    public function store(StoreRequest $request)
    {
        $store = $this->commonsettingRepository->store($request);
        return response()->json($store);
    }

    public function edit($id)
    {
        $common_setting = CommonSetting::find($id);
        return response()->json(['common_setting' => $common_setting]);
    }

    public function update(UpdateRequest $request)
    {
        $update = $this->commonsettingRepository->update($request);
        return response()->json($update);
    }

    public function destroy(Request $request)
    {
        $propertyFeatures = CommonSetting::where('id',$request->id)->delete();
        return response()->json(1);
    }
}
