<?php

namespace App\Http\Controllers\Collage;

use App\DataTables\ReserveQuotaConfirmDataTable;
use App\DataTables\ReserveQuotaDataTable;
use App\Http\Controllers\Controller;
use App\Models\Addmission;
use App\Models\AddmissionConfirmations;
use App\Models\College;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class ReservedQuotaController extends Controller
{
    public function index(ReserveQuotaDataTable $reservequotaDataTable)
    {
        return  $reservequotaDataTable->render('collage.reserved.index');
    }

    public function index2(ReserveQuotaConfirmDataTable $reservequotaconfirmDataTable)
    {
        return  $reservequotaconfirmDataTable->render('collage.reserved.index2');
    }

    public function quotaAdmissionApprove(Request $request)
    {
        $college = College::where('id', Auth()->user()->id)->first();
        $admission = Addmission::with('User')->where('user_id', $request->user)->first();
        $id = $request['id'];
        $status = Addmission::find($id);
        $user_id = User::where('id', $status['user_id'])->first();
        if ($status->status == "3") {
            $status->status = "1";
            AddmissionConfirmations::create([
                'addmission_id' => $status->id,
                'confirm_college_id' => Auth::guard('collage')->user()->id,
                'confirm_round_id' => $status->merit_round_id,
                'confirm_merit' => $status->merit,
                'confirmation_type' => 'R',
            ]);
        }
        $status->save();

        $data = ['title' => $status, 'admission' => $admission];
        $user['to'] = $admission->User->email;
        Mail::send('collage.mails.mail', $data, function ($message) use ($user) {
            $message->to($user['to']);
            $message->subject('Hello');
        });

        return $status;
    }
}
