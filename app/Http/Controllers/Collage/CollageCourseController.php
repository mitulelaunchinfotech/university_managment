<?php

namespace App\Http\Controllers\Collage;

use App\DataTables\CollegeCourseDataTable;
use App\Http\Controllers\Controller;
use App\Http\Requests\CollageCourse\StoreRequest;
use App\Http\Requests\CollageCourse\UpdateRequest;
use App\Models\College;
use App\Models\CollegeCourse;
use App\Models\Course;
use Illuminate\Http\Request;


class CollageCourseController extends Controller
{
    public function index(CollegeCourseDataTable $collegecourseDataTable)
    {
        return  $collegecourseDataTable->render('collage.course.index') ;     
    }

    public function create()
    {
        $college = College::select('id','name')->get();
        $course =   Course::select('id','name')->get();
        return view('collage.course.create',compact('college','course'));
    }

    public function store(StoreRequest $request)
    {
        $college_course = new CollegeCourse;
        $college_course->college_id = request()->user()->id;
        $college_course->course_id = $request->course_id;
        $college_course->reserved_seat = $request->reserved_seat;
        $college_course->merit_seat = $request->merit_seat;
        $college_course->seat_no = $request->reserved_seat + $request->merit_seat;
        $college_course->save();
        return response()->json('1');
    }

    public function edit($id)
    {
        $college_course = CollegeCourse::find($id);              
        return response()->json(['college_course'=>$college_course]);
    }

    public function update(UpdateRequest $request)
    {
        $college_course = CollegeCourse::find($request->id);
        $college_course->college_id = $request->college_id;
        $college_course->course_id = $request->course_id;
        $college_course->seat_no = $request->seat_no;
        $college_course->reserved_seat = $request->reserved_seat;
        $college_course->merit_seat = $request->merit_seat;
        $college_course->save();
        return response()->json(['college_course'=>$college_course]);        
    }

    public function destroy(Request $request)
    {
        $collage_course = CollegeCourse::where('id',$request->id)->delete();
        return response()->json(1);
    }

}
