<?php

namespace App\Http\Controllers\Collage;

use App\Http\Controllers\Controller;
use App\Models\College;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class CollegeController extends Controller
{
    public function profile()
    {
        $admin = Auth::guard('collage')->user();
        return view('collage.profile',compact('admin'));
    }

    public function updateprofile(Request $request)
    {
        $admin = Auth::guard('collage')->user();
        dd($admin);
        if ($request->hasFile('image')) {
            $destination = 'app/public/' . $admin->image;
            if (File::exists($destination)) {
                File::delete($destination);
            }
            $file = $request->file('image');
            $name = $file->getClientOriginalName();
            $file->move('app/public/', $name);
            $admin->image = $name;
        }
        $admin->name = $request->name;
        $admin->save();
        session()->flash('update','Profile updated successfully');
        return redirect()->back();  
    }

    public function changepassword()
    {
        return view('collage.change_password');
    }

    public function updatepassword(Request $request)
    {
        $hashedPassword = Auth::guard('collage')->user()->password;
        if (Hash::check($request->oldpassword, $hashedPassword)) {
            $update = bcrypt($request->newpassword);
            $users = Auth::guard('collage')->id();
            $id = College::where('id', $users)->update(['password' => $update]);
            if ($id) {
                session()->flash('update', 'password updated successfully');
            }
            return redirect()->back();
        } else {
            session()->flash('error', 'old password doesnt matched');
            return redirect()->back();
        }
    }
}
