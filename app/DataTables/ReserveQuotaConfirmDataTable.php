<?php

namespace App\DataTables;

use App\Models\Addmission;
use App\Models\CollegeMerit;
use App\Models\ReserveQuotaConfirm;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class ReserveQuotaConfirmDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('action', function ($data) {
                $btn = '<a  data-id="' . $data->id . '" class="edit btn btn-danger btn-sm btndelete "><i class="fa fa-trash"></i></a>';
                return $btn;
            })
            ->editColumn('user_id', function ($data) {
                return $data->User->name;
            })
            ->editColumn('course_id', function ($data) {
                if ($data->course_id == NULL) {
                    return '-';
                } else {
                    return $data->Course->name;
                }
            })
            ->editColumn('status', function ($data) {
                if ($data->status == 1) {
                    return '<a data-id="' . $data->id . '"  class="btn btn-primary status btn-xs">Confirm</a>';
                } else if ($data->status == 1) {
                    return '<a data-id="' . $data->id . '"  class="btn btn-primary status btn-xs">Confirm</a>';
                }
            })
            ->rawColumns(['action', 'user_id', 'course_id', 'status'])
            ->addIndexColumn();
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\ReserveQuotaConfirm $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Addmission $model)
    {
        $college_merit = CollegeMerit::where('college_id', Auth::user()->id)->first();
        $college_id = Auth::user()->id;
        if ($college_merit) {
            return
                $model->where('merit', '<=', $college_merit->merit)
                ->where('college_id', 'like', '%["' . $college_id . '"%')
                ->where('status', 1)
                ->newQuery();
        } else {
            return $model->newQuery();
        }
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->setTableId('reservequotaconfirm-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->dom('Bfrtip')
            ->orderBy(1)
            ->buttons(
                Button::make('create'),
                Button::make('export'),
                Button::make('print'),
                Button::make('reset'),
                Button::make('reload')
            );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('No')->data('DT_RowIndex')->searchable(false)->orderable(false),
            Column::make('user_id')->title('Student'),
            Column::make('merit'),
            Column::make('course_id')->title('Course'),
            Column::make('merit_round_id'),
            // Column::make('college_id'),
            Column::make('addmission_code'),
            Column::make('addmission_date'),
            Column::make('status'),

        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'ReserveQuotaConfirm_' . date('YmdHis');
    }
}


