<?php

namespace App\DataTables;

use App\Models\User;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class UserDataTable extends DataTable
{    
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('action', function($data){
                $btn = "";                
                $btn .='<a  data-id="' . $data->id . '" class="edit btn btn-danger btn-sm btndelete "><i class="fa fa-trash"></i></a>';
                return $btn;
            })
            ->editColumn('image', function ($data) {
                return '<img src="' . $data->image . '" width="50px" height="50px">';
            })
            ->editColumn('gender', function ($data) {
                if($data->gender == 'F')
                {
                    return '<span>Female</span>';
                }elseif($data->gender == 'M')
                {
                    return '<span>Male</span>';
                }else{
                    return '<span>Other</span>';
                }
            })
            ->rawColumns(['action','image','gender'])
            ->addIndexColumn();
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(User $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
        ->setTableId('user-table')
        ->columns($this->getColumns())
        ->minifiedAjax()
        ->dom('Bfrtip')
        ->orderBy(1)
        ->pageLength(25)
        ->buttons(
            Button::make('excel'),
            Button::make('pdf'),
        );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('No')->data('DT_RowIndex')->searchable(false)->orderable(false),
            Column::make('name'),
            Column::make('email'),
            Column::make('contact_no'),
            Column::make('address'),
            Column::make('image'),
            Column::make('gender'),
            Column::computed('action')
            ->exportable(false)
            ->printable(false)
            ->width(60)
                ->addClass('text-center'),            
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'User_' . date('YmdHis');
    }
}
