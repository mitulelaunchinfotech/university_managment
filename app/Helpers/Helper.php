<?php
//Image Upload
if (!function_exists('uploadFile')) {
    function uploadFile($file, $dir)
    {
        if ($file) {
            $destinationPath =  storage_path('app/public') . DIRECTORY_SEPARATOR . $dir . DIRECTORY_SEPARATOR;
            $media_image = $file->hashName();
            $file->move($destinationPath, $media_image);
            return $media_image;
        }
    }
}


if (!function_exists('getRandomString')) {
    function getRandomString($length = 6)
    {
        $characters = '0123456789';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}