<?php
namespace App\Repository;

use App\Interfaces\CommonSettingInterface;
use App\Models\College;
use App\Models\CommonSetting;
use Illuminate\Support\Facades\Hash;

class  CommonSettingRepository implements CommonSettingInterface
{
    public function store($array)
    {
        $common_setting = new CommonSetting;
        $common_setting->subject_id = $array->subject_id;
        $common_setting->marks = $array->marks;
        $common_setting->save();
        return $common_setting;
    }    

    public function update($array)
    {
        $common_setting = CommonSetting::find($array->id);
        $common_setting->marks = $array->marks;
        $common_setting->save();        
        return $common_setting;
        
    }
}
?>

