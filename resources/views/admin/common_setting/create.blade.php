@extends('layouts.admin.master')
@section('title','Common-Setting')
@push('css')

<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" rel="stylesheet" />
@endpush
@section('content')


<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Common Setting
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">College</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                    </div>
                    <form id="myform" method="post">
                        
                            <div class="form-group">
                                <label>Subject</label>
                                <select class="form-control" name="subject_id" id="subject_id">
                                    <option>Select Subject</option>
                                    @foreach($subject as $sub)
                                    <option value="{{$sub->id}}">{{$sub->name}}</option>
                                    @endforeach
                                </select>

                            </div>

                            <div class="form-group">
                                <label>Marks</label>
                                <input type="email" class="form-control" name="marks" placeholder="Enter Marks ">
                            </div>

                            <div class="row">
                                <div class="box-footer">
                                    <button type="submit" id="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                    </form>
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
</div>
<div class="control-sidebar-bg"></div>
</div>
@endsection
@push('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
<script>
    $(document).ready(function() {
        $("#submit").click(function(e) {
            e.preventDefault();
            var form = $("#myform")[0];
            var data = new FormData(form);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{route("admin.common_setting.store")}}',
                type: 'post',
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                dataType: 'JSON',
                success: function(result) {
                    swal({
                        title: "Inserted",
                        text: "Insert Succesfully!",
                        closeOnConfirm: false,
                        closeOnCancel: false,
                        buttons: ["Cancel", "Submit"]
                    }).then(function(isConfirm) {
                        if (isConfirm) {
                            $('#myform')[0].reset();
                            window.location.href = '{{route("admin.common_setting.index")}}';
                        } else {
                            swal("Cancelled", "", "error")
                        }
                    });
                },
                error: function(result) {
                    $.each(result.responseJSON.errors, function(i, error) {
                        $('.' + i + 'error').empty();
                        var el = $(document).find('[name="' + i + '"]');
                        el.after($('<span class="' + i + 'error" style="color: red;">' + error[0] + '</span>'));
                    });
                }
            })
        });
    })

    //Store Year
    $("#datepicker").datepicker({
        format: "yyyy",
        viewMode: "years",
        minViewMode: "years"
    });
</script>

@endpush