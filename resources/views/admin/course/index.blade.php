@extends('layouts.admin.master')
@section('title','Category')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Course List
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
            <li><a href="#">Course</a></li>
            <li class="active">List</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="modal fade" id="bookModal">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">Update Book</h4>
                            </div>
                            <div class="modal-body">
                                <form id="editform" enctype="multipart/form-data">
                                    @csrf
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label>Name</label>
                                            <input type="text" class="form-control" name="name" id="name" placeholder="Enter Name">
                                        </div>

                                        <div class="form-group">
                                            <label>Select</label>
                                            <select class="form-control">
                                                <option>Active</option>
                                                <option>Inactive</option>
                                            </select>
                                        </div>
                                    </div>
                                    <input type="hidden" id="id" name="id">
                                </form>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
                                    <button type="button" id="update" name="update" class="btn btn-primary">Update</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box">
                    <div class="box-body">
                        <!-- <a href="{{route('admin.college.create')}}" id="update" name="update" style="float: right;margin-top:10px" class="btn btn-primary"><i class="fa fa-plus"></i>Add College</a> -->
                        {!! $dataTable->table(['class' => 'table table-bordered dt-responsive nowrap']) !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="control-sidebar-bg"></div>

</div>
@endsection
@push('js')
{!! $dataTable->scripts() !!}
<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap4.min.js"></script>
<script>
    $(document).on("click", ".btnedit", function(e) {
        e.preventDefault();
        $('#bookModal').modal('show');
        var id = $(this).attr("data-id");
        var url = "{{ route('admin.course.edit',':id') }}";
        url = url.replace(':id', id);
        console.log(url);
        $.ajax({
            url: url,
            type: "GET",
            dataType: "JSON",
            success: function(data) {
                $("#id").val(data.course.id);
                $("#name").val(data.course.name);
            },
        });
    });

    $(document).on("click", ".btndelete", function(e) {
        e.preventDefault();
        var id = $(this).attr("data-id");
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "{{route('admin.college.destroy')}}",
            type: 'post',
            data: {
                id: id
            },
            dataType: "JSON",
            success: function(data) {
                swal({
                    title: 'Delete',
                    text: 'Are you sure delete succesfully ??',
                    buttons: ['Cancel', 'Delete']
                }).then(function(isConfirm) {
                    if (isConfirm) {
                        window.LaravelDataTables['book-table'].draw();
                        $("#myModal").modal('hide');
                    } else {
                        swal("Cancelled", "", "error");
                    }
                });
            }
        });
    });


    //Update
    $("#update").on('click', function(e) {
        e.preventDefault();
        var form = $("#editform")[0];
        var data = new FormData(form);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '{{route("admin.course.update")}}',
            data: data,
            type: 'post',
            cache: false,
            contentType: false,
            processData: false,
            dataType: "JSON",
            success: function() {
                swal({
                    title: 'Updated',
                    text: 'Updated Succesfully',
                    buttons: ['Cancel', 'Update']
                }).then(function(isConfirm) {
                    if (isConfirm) {
                        window.LaravelDataTables['course-table'].draw();
                        $("#bookModal").modal('hide');
                    } else {
                        swal("Cancelled", "", "error");
                    }
                });
            },
            error: function(data) {
                $.each(data.responseJSON.errors, function(key, value) {
                    $('input[name=' + key + ']').after('<span class="error" style="color:red">' + value + '</span>');
                });
            }
        });
    });
    $(document).on('click', '#edit_btn', function() {
    });
</script>

@endpush