@extends('layouts.admin.master')
@section('title','Merit')
@push('css')

<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" rel="stylesheet" />
@endpush
@section('content')
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Merit
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">Merit</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                    </div>
                    <form id="myform" method="post">
                        <div class="form-group">
                            <label>Round No</label>
                            <input type="text" class="form-control" name="round_no" placeholder="Enter Merit Round ">
                        </div>

                        <div class="form-group">
                            <label>Course</label>
                            <select class="form-control" name="course_id" id="course_id">
                                <option>Select Course</option>
                                @foreach($course as $c)
                                <option value="{{$c->id}}">{{$c->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Start Date</label>
                            <input type="text" class="form-control" name="fromdate" id="fromDate" placeholder="Enter start date">
                        </div>

                        <div class="form-group">
                            <label>End Date</label>
                            <input type="text" class="form-control" name="todate" id="toDate" placeholder="Enter end date ">
                        </div>


                        <div class="form-group">
                            <label>Result Declare Date</label>
                            <input type="text" class="form-control" name="resultdate" id="datepicker4" placeholder="Enter result de date ">
                        </div>

                        <div class="form-group">
                            <label>Subject</label>
                            <select class="form-control" name="status" id="status">
                                <option>Status</option>
                                <option value="1">Active</option>
                                <option value="0">Deactive</option>
                            </select>
                        </div>

                        <div class="row">
                            <div class="box-footer">
                                <button type="submit" id="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>
<div class="control-sidebar-bg"></div>
</div>
@endsection
@push('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
<script>
    $(document).ready(function() {
        $("#submit").click(function(e) {
            e.preventDefault();
            var form = $("#myform")[0];
            var data = new FormData(form);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{route("admin.merit.store")}}',
                type: 'post',
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                dataType: 'JSON',
                success: function(result) {
                    swal({
                        title: "Inserted",
                        text: "Insert Succesfully!",
                        closeOnConfirm: false,
                        closeOnCancel: false,
                        buttons: ["Cancel", "Submit"]
                    }).then(function(isConfirm) {
                        if (isConfirm) {
                            $('#myform')[0].reset();
                            window.location.href = '{{route("admin.merit.index")}}';
                        } else {
                            swal("Cancelled", "", "error")
                        }
                    });
                },
                error: function(result) {
                        $.each(result.responseJSON.errors, function(i, error) {
                            $('.' + i + 'error').empty();
                            var el = $(document).find('[name="' + i + '"]');
                            el.after($('<span class="' + i + 'error" style="color: red;">' + error[0] + '</span>'));
                        });
                }
            })
        });
    })

    //Store Date
    $('#datepicker4').datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd',
        startDate: new Date() 
    });

    $(document).ready(function() {
        $("#fromDate").datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
        }).on('changeDate', function(selected) {
            var minDate = new Date(selected.date.valueOf());
            $('#toDate').datepicker('setStartDate', minDate);
        });

        $("#toDate").datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
        }).on('changeDate', function(selected) {
            var minDate = new Date(selected.date.valueOf());
            $('#fromDate').datepicker('setEndDate', minDate);
        });
    });
</script>

@endpush