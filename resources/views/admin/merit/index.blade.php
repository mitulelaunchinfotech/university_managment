@extends('layouts.admin.master')
@section('title','Merit Round')
@push('css')
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" rel="stylesheet" />
@endpush
@section('content')
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Merit Round List
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
            <li><a href="#">MeritRound</a></li>
            <li class="active">List</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="modal fade" id="bookModal">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">Update Merit Round</h4>
                            </div>
                            <div class="modal-body">
                                <form id="editform" enctype="multipart/form-data">
                                    @csrf

                                    <div class="form-group">
                                        <label>Round No</label>
                                        <input type="text" class="form-control" name="round_no" id="round_no" placeholder="Enter Merit Round ">
                                    </div>

                                    @php
                                    $course = DB::table('courses')->select('id','name')->get();
                                    @endphp
                                    <div class="form-group">
                                        <label>Course</label>
                                        <select class="form-control" name="course_id" id="course_id">
                                            <option>Select Course</option>
                                            @foreach($course as $c)
                                            <option value="{{$c->id}}">{{$c->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label>Start Date</label>
                                        <input type="text" class="form-control" name="fromDate" id="fromDate" placeholder="Enter start date">
                                    </div>

                                    <div class="form-group">
                                        <label>End Date</label>
                                        <input type="text" class="form-control" name="toDate" id="toDate" placeholder="Enter end date ">
                                    </div>


                                    <div class="form-group">
                                        <label>Result Declare Date</label>
                                        <input type="text" class="form-control" name="resultdate" id="datepicker4" placeholder="Enter result declare date ">
                                    </div>

                                    <div class="form-group">
                                        <label>Subject</label>
                                        <select class="form-control" name="status" id="status">
                                            <option>Status</option>
                                            <option value="1">Active</option>
                                            <option value="0">Deactive</option>
                                        </select>
                                    </div>
                                    <input type="hidden" id="id" name="id">
                                </form>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
                                    <button type="button" id="update" name="update" class="btn btn-primary">Update</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box">
                    <div class="box-body">
                        <a href="{{route('admin.merit.create')}}" id="update" name="update" style="float: right;margin-top:10px" class="btn btn-primary"><i class="fa fa-plus"></i>Add Merit Round</a>
                        {!! $dataTable->table(['class' => 'table table-bordered dt-responsive nowrap']) !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="control-sidebar-bg"></div>

</div>
@endsection
@push('js')
{!! $dataTable->scripts() !!}
<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
<script>
    $(document).on("click", ".btnedit", function(e) {
        e.preventDefault();
        $('#bookModal').modal('show');
        var id = $(this).attr("data-id");
        var url = "{{route('admin.merit.edit',':id')}}";
        url = url.replace(':id', id);
        console.log(url);
        $.ajax({
            url: url,
            type: "GET",
            dataType: "JSON",
            success: function(data) {
                $("#id").val(data.merit.id);
                $("#round_no").val(data.merit.round_no);
                $("#course_id").val(data.merit.course_id);
                $("#fromDate").val(data.merit.start_date);
                $("#toDate").val(data.merit.end_date);
                $("#datepicker4").val(data.merit.merit_result_declare_date);
                $("#status").val(data.merit.status);
            },
        });
    });

    $(document).on("click", ".btndelete", function(e) {
        e.preventDefault();
        var id = $(this).attr("data-id");
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "{{route('admin.merit.destroy')}}",
            type: 'post',
            data: {
                id: id
            },
            dataType: "JSON",
            success: function(data) {
                swal({
                    title: 'Delete',
                    text: 'Are you sure delete succesfully ??',
                    buttons: ['Cancel', 'Delete']
                }).then(function(isConfirm) {
                    if (isConfirm) {
                        window.LaravelDataTables['meritround-table'].draw();
                        $("#myModal").modal('hide');
                    } else {
                        swal("Cancelled", "", "error");
                    }
                });
            }
        });
    });


    //Update
    $("#update").on('click', function(e) {
        e.preventDefault();
        var form = $("#editform")[0];
        var data = new FormData(form);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '{{route("admin.merit.update")}}',
            data: data,
            type: 'post',
            cache: false,
            contentType: false,
            processData: false,
            dataType: "JSON",
            success: function() {
                swal({
                    title: 'Updated',
                    text: 'Updated Succesfully',
                    buttons: ['Cancel', 'Update']
                }).then(function(isConfirm) {
                    if (isConfirm) {
                        window.LaravelDataTables['meritround-table'].draw();
                        $("#bookModal").modal('hide');
                    } else {
                        swal("Cancelled", "", "error");
                    }
                });
            },
            error: function(data) {
                $.each(data.responseJSON.errors, function(i, error) {
                        $('.' + i + 'error').empty();
                        var el = $(document).find('[name="' + i + '"]');
                        el.after($('<span class="' + i + 'error" style="color: red;">' + error[0] + '</span>'));
                    });
            }
        });

    });
    $(document).on('click', '#edit_btn', function() {});


    $('#datepicker4').datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd',
        startDate: new Date()
    });

    $(document).ready(function() {
        $("#fromDate").datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
        }).on('changeDate', function(selected) {
            var minDate = new Date(selected.date.valueOf());
            $('#toDate').datepicker('setStartDate', minDate);
        });

        $("#toDate").datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
        }).on('changeDate', function(selected) {
            var minDate = new Date(selected.date.valueOf());
            $('#fromDate').datepicker('setEndDate', minDate);
        });
    });
</script>

@endpush