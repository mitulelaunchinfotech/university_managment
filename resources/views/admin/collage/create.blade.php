@extends('layouts.admin.master')
@section('title','Book')
@push('css')

<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"/>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" rel="stylesheet"/>
@endpush
@section('content')


<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      College
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Forms</a></li>
      <li class="active">College</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Add College</h3>
          </div>
          <form id="myform" method="post">
                <div class="form-group ">
                <label>Name</label>
                    <input type="text" class="form-control" name="name" placeholder="Enter name">
                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                </div>

                <div class="form-group">
                <label>Email</label>
                    <input type="email" class="form-control" name="email" placeholder="Email">
                </div>

                <div class="form-group">
                <label>Mobile</label>
                    <input type="text" class="form-control" name="contact_no" placeholder="Mobile">
                    <span class="glyphicon glyphicon-mobile form-control-feedback"></span>
                </div>

                <div class="form-group">
                <label>Address</label>
                    <textarea class="form-control" rows="3" name="address" placeholder="Enter Address"></textarea>
                </div>
                
                <div class="form-group">
                    <label>Password</label>
                        <input type="password" class="form-control" name="password" placeholder="Password">
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>

                <div class="form-group">
                <label>Logo</label>
                    <input type="file" class="form-control" name="logo">
                    <span class="glyphicon glyphicon-mobile form-control-feedback"></span>
                </div>
                <div class="row">
                    <!-- /.col -->
                    <div class="box-footer">
                         <button type="submit" id="submit" class="btn btn-primary">Submit</button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>
        </div>
        <!-- /.box -->
      </div>
    </div>
  </section>
</div>
<div class="control-sidebar-bg"></div>
</div>
@endsection
@push('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
<script>
  $(document).ready(function() {
    $("#submit").click(function(e) {
      e.preventDefault();
      var form = $("#myform")[0];
      var data = new FormData(form);
      $.ajax({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: '{{route("admin.college.store")}}',
        type: 'post',
        data: data,
        cache: false,
        contentType: false,
        processData: false,
        dataType: 'JSON',
        success: function(result) {
          swal({
            title: "Inserted",
            text: "Insert Succesfully!",
            closeOnConfirm: false,
            closeOnCancel: false,
            buttons: ["Cancel", "Submit"]
          }).then(function(isConfirm) {
            if (isConfirm) {
              $('#myform')[0].reset();
              window.location.href = '{{route("admin.college.index")}}';
            } else {
              swal("Cancelled", "", "error")
            }
          });
        },
        error: function(result) {
          $.each(result.responseJSON.errors, function(i, error) {
            $('.' + i + 'error').empty();
            var el = $(document).find('[name="' + i + '"]');
            el.after($('<span class="' + i + 'error" style="color: red;">' + error[0] + '</span>'));
          });
        }
      })
    });
  })


  //Store Year

  $("#datepicker").datepicker({
    format: "yyyy",
    viewMode: "years",
    minViewMode: "years"
  });
</script>

@endpush    