@extends('layouts.student.master')
@section('title','Marks')
@push('css')
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" rel="stylesheet" />
@endpush
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Student Marks
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">Course</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add Mark</h3>
                    </div>
                    <!-- <form id="myform" method="post"> -->
                    {{ Form::open(['id'=>'myform','method'=>'post','enctype'=>'multipart/form-data'])  }}
                    
                    @foreach($subject as $cou)
                    <div class="form-group">
                        <label>{{$cou->name}}</label>
                        <input type="text" class="form-control" id="{{$cou->id}}" name="subject_id[{{$cou->id}}]" id="subject_id" placeholder="Enter subject">
                    </div>
                    @endforeach

                    <div class="form-group ">
                        <label>Total Mark</label>
                        <input type="text" class="form-control" name="total_mark" placeholder="Enter total mark">
                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                    </div>
                    
                    <div class="row">
                        <div class="box-footer">
                            <button type="submit" id="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                    <!-- </form> -->
                    {{ Form::close()   }}
                </div>
            </div>
        </div>
    </section>
</div>
<div class="control-sidebar-bg"></div>
</div>
@endsection
@push('js')
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.0/jquery.validate.min.js"></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.0/additional-methods.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
<script>
    $(document).ready(function() {
        $("#submit").click(function(e) {
            e.preventDefault();
            var form = $("#myform")[0];
            var data = new FormData(form);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{route("student.mark.store")}}',
                type: 'post',
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                dataType: 'JSON',
                success: function(result) {
                    swal({
                        title: "Inserted",
                        text: "Insert Succesfully!",
                        closeOnConfirm: false,
                        closeOnCancel: false,
                        buttons: ["Cancel", "Submit"]
                    }).then(function(isConfirm) {
                        if (isConfirm) {
                            $('#myform')[0].reset();
                            window.location.href = '{{route("student.mark.index")}}';
                        } else {
                            swal("Cancelled", "", "error")
                        }
                    });
                },
                error: function(result) {
                    $.each(result.responseJSON.errors, function(i, error) {
                        $('.' + i + 'error').empty();
                        var el = $(document).find('[name="' + i + '"]');
                        el.after($('<span class="' + i + 'error" style="color: red;">' + error[0] + '</span>'));
                    });
                }
            })
        });
    })

    $('#myform').validate({
        rules: {
            "subject_id[]": {
                mytst: true
            },
            password: {
                required: true
            }
        },
        messages: {

        },
        submitHandler: function(form) {
            submit.form();
        }
    });


    $.validator.addMethod("mytst", function(value, element) {
        var flag = true;
        $("[name^=subject_id]").each(function(i, j) {
            $(this).parent('p').find('label.error').remove();
            $(this).parent('p').find('label.error').remove();
            if ($.trim($(this).val()) == '') {
                flag = false;
                $(this).parent('p').append('<label  id="subject_id' + i + '-error" class="error">This field is required.</label>');
            }
        });
        return flag;
    }, "");

    var j = 1;
    $('input[type="button"]').click(function() {
        $('form div').append('<p><input name="subject_id[]" id="subject_id' + j + '" type="text" class="form-control" ' + j + ' Name " ></p>');
        j++;
    });
</script>

@endpush