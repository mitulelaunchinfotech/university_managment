@extends('layouts.student.master')
@section('title','ChangePassword')
@push('css')
<style>
    .error{
        color:red;
    }
</style>
@endpush
@section('content')
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Change Password
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Examples</a></li>
            <li class="active">Change Password</li>
        </ol>
    </section>
    <section class="content ">
        <div class="row justify-content-center ">
            <div class="col-md-6 form-group col-md-offset-3 align-center">
                @if($message = Session::get('update'))
                <div class="alert alert-success" class="close" id="success">
                    {{$message}}
                </div>
                @endif

                @if($message = Session::get('error'))
                <div class="alert alert-danger" class="close" id="success">
                    {{$message}}
                </div>
                @endif
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li><a href="#settings" data-toggle="tab">ChangePassword</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="settings">
                            <form class="form-horizontal" id="changepass" method="post" action="" id="profileupdate">
                                @csrf
                                <div class="form-group">
                                    <label for="inputEmail" class="col-sm-2 control-label">OldPassword</label>
                                    <div class="col-sm-10">
                                        <input type="password" class="form-control" id="inputEmail" name="oldpassword" placeholder="Enter Old Password">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputEmail" class="col-sm-2 control-label">NewPassword</label>
                                    <div class="col-sm-10">
                                        <input type="password" class="form-control" id="inputPassword" name="newpassword" placeholder="Enter New Password">
                                    </div>
                                </div>


                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10">
                                        <button type="submit" id="submit" class="btn btn-danger">change Password</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<div class="control-sidebar-bg"></div>
</div>
@endsection
@push('js')
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.0/jquery.validate.min.js"></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.0/additional-methods.js"></script>
<script>    
    $('#changepass').validate({
        rules: {
            oldpassword: {
                required: true
            },
            newpassword: {
                required: true
            }
        },
        messages: {
            oldpassword: {
                required: "Please enter oldpassword"
            },
            newpassword: {
                required: "Please enter newpassword"
            }
        },
        submitHandler: function(form) {
            submit.form();
        }
    });
</script>
@endpush