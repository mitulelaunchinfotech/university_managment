<!DOCTYPE html>
<html>
<meta http-equiv="content-type" content="text/html;charset=utf-8" />

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>UMS | Log in</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="{{asset('asset/css/bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{asset('asset/css/font-awesome.min.css')}}">
  <link rel="stylesheet" href="{{asset('asset/css/ionicons.min.css')}}">
  <link rel="stylesheet" href="{{asset('asset/css/AdminLTE.min.css')}}">
  <link rel="stylesheet" href="{{asset('asset/css/blue.css')}}">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>

<body class="hold-transition login-page">
  <div class="login-box">
    <div class="login-logo">
      <a href="../../index2.html"><b>Student</b></a>
    </div>
    <div class="login-box-body">
      <p class="login-box-msg">Sign in to start your session</p>
      <form action="{{ route('student.login') }}" id="myform" method="post">
        @csrf
        <div class="form-group has-feedback">
          <input type="email" class="form-control" name="email" placeholder="Email">
        </div>
        @if(session()->has('message'))          
          <span class="invalid-feedback" role="alert">
            <strong class="error">{{ session()->get('message') }}</strong>
          </span>
        @endif
        <div class="form-group has-feedback">
          <input type="password" class="form-control" name="password" placeholder="Password">
        </div>
        @error('password')
        <span class="invalid-feedback" role="alert">
          <strong>{{ $message }}</strong>
        </span>
        @enderror
        <div class="row">
          <div class="col-xs-4">
            <button type="submit" name="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
          </div>
        </div>
      </form>
      <a href="#">I forgot my password</a><br>
      <a href="{{route('student.register')}}" class="text-center">Register a new membership</a>

    </div>
  </div>
  <script src="{{asset('asset/js/jquery.min.js')}}"></script>
  <script src="{{asset('asset/js/bootstrap.min.js')}}"></script>
  <script src="{{asset('asset/js/icheck.min.js')}}"></script>
</body>

</html>
  <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.0/jquery.validate.min.js"></script>
  <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.0/additional-methods.js"></script>
  <script>
    $('#myform').validate({
      rules: {
        email: {
          required: true
        },
        password: {
          required: true
        }
      },
      messages: {
        email: {
          required: "Please enter email"
        },
        password: {
          required: "Please enter password"
        }
      },
      submitHandler: function(form) {
        submit.form();
      }
    });
  </script>