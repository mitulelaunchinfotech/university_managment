@extends('layouts.student.master')
@section('title','Profile')
@push('css')
<style>
    .error {
        color: red;
    }
</style>
@endpush
@section('content')
<div class="content-wrapper">
    <section class="content-header">
        <h1>Student Profile</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Examples</a></li>
            <li class="active">Student profile</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content ">
        <div class="row justify-content-center">
            <!-- /.col -->
            <div class="col-md-6 form-group col-md-offset-3 align-center">
                @if($message = Session::get('update'))
                <div class="alert alert-success" class="close" id="success">
                    {{$message}}
                </div>
                @endif
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li><a href="#settings" data-toggle="tab">Profile</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="settings">
                            <form class="form-horizontal" method="post" action="{{route('student.student.profile')}}" id="profileupdate">
                                @csrf
                                <div class="form-group" style="text-align:center">
                                    <div class="col-sm-12">
                                        <img src="{{$admin->image}}" id="upload-img" width="100px" height="100px"></img>
                                    </div>
                                    <label for="inputName" class="col-sm-2 control-label">Image</label>
                                    <div class="col-sm-10">
                                        <input type="file" class="form-control" name="image" name="name">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputName" class="col-sm-2 control-label">Name</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="name" value="{{$admin->name}}" id="inputName" placeholder="Name">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail" class="col-sm-2 control-label">Email</label>
                                    <div class="col-sm-10">
                                        <input type="email" class="form-control" value="{{$admin->email}}" readonly id="inputEmail" placeholder="Email">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputEmail" class="col-sm-2 control-label">Contact_no</label>
                                    <div class="col-sm-10">
                                        <input type="email" class="form-control" value="{{$admin->contact_no}}" readonly id="inputEmail" placeholder="Email">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputAddress" class="col-sm-2 control-label">Address</label>
                                    <div class="col-sm-10">
                                        <textarea type="text" class="form-control" name="address" id="inputAddress">{{$admin->address}}</textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputEmail" class="col-sm-2 control-label">Adhaar_Number</label>
                                    <div class="col-sm-10">
                                        <input type="email" class="form-control" value="{{$admin->adhaar_card_no}}" readonly id="inputEmail" placeholder="Email">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputDob" class="col-sm-2 control-label">DOB</label>
                                    <div class="col-sm-10">
                                        <input type="date" class="form-control" value="{{ \Carbon\Carbon::parse($admin->dob)->format('Y-m-d') }}" id="inputDob" placeholder="DOB">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10">
                                        <button type="submit" id="update" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<div class="control-sidebar-bg"></div>
</div>
@endsection

@push('js')
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.0/jquery.validate.min.js"></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.0/additional-methods.js"></script>
<script>
    //Image upload before preview
    $(":file").change(function() {
        if (this.files && this.files[0]) {
            var reader = new FileReader();
            reader.onload = imageIsLoaded;
            reader.readAsDataURL(this.files[0]);
        }
    });

    function imageIsLoaded(e) {
        $("#upload-img").attr('src', e.target.result);
    }


    //Profile validation
    $('#profileupdate').validate({
        rules: {
            name: {
                required: true
            },
            address: {
                required: true
            }
        },
        messages: {
            name: {
                required: "Please enter name"
            },
            address: {
                required: "Please enter address"
            }
        },
        submitHandler: function(form) {
            submit.form();
        }
    });
</script>
@endpush