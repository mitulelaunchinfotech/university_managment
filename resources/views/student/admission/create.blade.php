@extends('layouts.student.master')
@section('title','Admission')
@push('css')
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" rel="stylesheet" />
@endpush
@section('content')
<div class="content-wrapper">

    <section class="content">
        <div class="row justify-content-center">
            <div class="col-md-6 form-group col-md-offset-3 align-center">
                <div class="box box-primary ">
                    <div class="box-header with-border">
                        <h3 class="box-title">Admission</h3>
                    </div>
                    @if($form == NULL)
                        <div class="box-header with-border">
                            <h3 class="box-title" style="color:red">Form are now closed</h3>
                        </div>
                    @else
                    <form id="myform" method="post">
                        <!-- <div class="form-group ">
                            <label>Merit</label>
                            <input type="text" class="form-control" name="merit" placeholder="Enter merit">
                            <span class="glyphicon glyphicon-user form-control-feedback"></span>
                        </div> -->

                        <!-- <div class="form-group ">
                            <label>College_id</label>
                            <input type="text" class="form-control" name="college_id[]" placeholder="Enter collage">
                            <span class="glyphicon glyphicon-user form-control-feedback"></span>
                        </div> -->

                        <div class="form-group">
                            <label>College</label>
                            <select class="form-control" name="college_id[]" multiple id="college_id">
                                <option>--Select Collage--</option>
                                @foreach($collage as $c)
                                <option value="{{$c->id}}">{{$c->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Admission Date</label>
                            <input type="text" class="form-control" name="addmission_date" id="datepicker4" placeholder="Enter start date">
                        </div>

                        <div class="form-group">
                            <label>Course</label>
                            <select class="form-control" name="course_id" id="course_id">
                                <option>Select Course</option>
                                @foreach($course as $c)
                                <option value="{{$c->id}}">{{$c->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Merit Round</label>
                            <select class="form-control" name="merit_round_no" id="merit_round_no">
                                <option>Select Course</option>
                                @foreach($merit_round as $m)
                                <option value="{{$m->id}}">{{$m->round_no}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="row">
                            <div class="box-footer">
                                <button type="submit" autocomplete="off" id="submit" class="btn btn-primary btnsubmit">Submit</button>
                            </div>
                        </div>
                    </form>
                    @endif
                </div>              
            </div>
        </div>
    </section>
</div>
<div class="control-sidebar-bg"></div>
</div>
@endsection
@push('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
<script>
    $(document).ready(function() {
        $("#submit").click(function(e) {
            e.preventDefault();
            var form = $("#myform")[0];
            var data = new FormData(form);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{route("student.admission.store")}}',
                type: 'post',
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                dataType: 'JSON',
                success: function(result) {
                    swal({
                        title: "Inserted",
                        text: "Insert Succesfully!",
                        closeOnConfirm: false,
                        closeOnCancel: false,
                        buttons: ["Cancel", "Submit"]
                    }).then(function(isConfirm) {
                        if (isConfirm) {
                            $('#myform')[0].reset();
                            window.location.href = '{{route("student.admission.index")}}';
                        } else {
                            swal("Cancelled", "", "error")
                        }
                    });
                    $("#submit").attr("disabled", true);
                },
                error: function(result) {
                    $.each(result.responseJSON.errors, function(i, error) {
                        $('.' + i + 'error').empty();
                        var el = $(document).find('[name="' + i + '"]');
                        el.after($('<span class="' + i + 'error" style="color: red;">' + error[0] + '</span>'));
                    });
                }
            })
        });
    })


    //Store Date
    $('#datepicker4').datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd',
        startDate: new Date()
    });
</script>

@endpush