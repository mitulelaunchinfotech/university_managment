@extends('layouts.student.master')
@section('title','Admission')
@push('css')
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" rel="stylesheet" />
@endpush
@section('content')
<div class="content-wrapper">

    <section class="content ">
        <div class="row justify-content-center">
            <div class="col-md-6 form-group col-md-offset-3 align-center">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Admission</h3>
                    </div>
                   @if($status)
                   <div class="box-header with-border">
                        <h3 class="box-title" style="color:red;align:center">You will appear next round</h3>
                    </div>
                    @elseif($status1)
                    <div class="box-header with-border">
                        <h3 class="box-title" style="color:red;align:center">Rejected</h3>
                    </div>              
                    @else
                    <div id="messages"></div>
                    <div id="message"></div>
                    <form id="myform" method="post">
                        <div class="form-group">
                            <label>Merit</label>
                            <input type="text" value="{{$admission->merit}}" name="merit" class="form-control" readonly>
                        </div>

                        <div class="form-group">
                            <label>Course</label>
                            <input type="text" value="{{$admission->Course->name}}" name="course_id" class="form-control" readonly>
                        </div>

                        <div class="form-group">
                            <label>Merit Round</label>
                            <input type="text" value="{{$admission->merit_round_id}}" name="merit_round_id" class="form-control" readonly>
                        </div>

                        <div class="form-group">
                            <label>Admission Date</label>
                            <input type="text" value="{{$admission->addmission_date}}" name="addmission_date" class="form-control" readonly>
                        </div>

                        <div class="form-group">
                            <label>Admission Code</label>
                            <input type="text" value="{{$admission->addmission_code}}" name="addmission_code" class="form-control" readonly>
                        </div>

                        <div class="form-group">
                            <label>College</label>
                            <select class="form-control" name="college_id" id="college_id">
                                <option value="{{$admission->college_id[0]}}">{{$admission->college->name}}</option>
                            </select>
                        </div>

                        <input type="hidden" value="{{$admission->id}}" name="admission_id">
                        <div class="modal-footer">
                            <button type="button" class="btn btn-success status" data-id="{{$admission->id}}" data-status="1" id="confirm" name="confirm" data-dismiss="modal">Confirm</button>
                            <button type="button" id="next" data-id="{{$admission->id}}" name="next" data-status="0" class="btn btn-primary status">Next Round</button>
                            <button type="button" id="reject" data-id="{{$admission->id}}" data-status="2" name="reject" class="btn btn-danger status">Reject</button>
                        </div>
                    </form>
                    @endif
                </div>
            </div>
        </div>
    </section>
</div>
<div class="control-sidebar-bg"></div>
</div>
@endsection
@push('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>

<script>
    $(document).on("click", '.btnshow', function(e) {
        e.preventDefault();
        var url = $(this).attr("href");
        $.ajax({
            url: url,
            type: "GET",
            dataType: "JSON",
            success: function(data) {
                $("#set_name").text(data.name);
                $("#set_email").text(data.email);
                $("#set_mobile").text(data.mobile);
                $("#set_image").attr('src', data.image);
            }
        });
    });

    //Store Admission Confirmation
    $(document).ready(function() {
        $("#confirm").click(function(e) {
            e.preventDefault();
            var form = $("#myform")[0];
            var data = new FormData(form);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{route("student.admission_confirm.store")}}',
                type: 'post',
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                dataType: 'JSON',
                success: function(result) {
                    swal({
                        title: "Inserted",
                        text: "Insert Succesfully!",
                        closeOnConfirm: false,
                        closeOnCancel: false,
                        buttons: ["Cancel", "Submit"]
                    }).then(function(isConfirm) {
                        if (isConfirm) {
                            $('#myform')[0].reset();
                        } else {
                            swal("Cancelled", "", "error")
                        }
                    });
                },
                error: function(result) {
                    $.each(result.responseJSON.errors, function(i, error) {
                        $('.' + i + 'error').empty();
                        var el = $(document).find('[name="' + i + '"]');
                        el.after($('<span class="' + i + 'error" style="color: red;">' + error[0] + '</span>'));
                    });
                }
            })
        });
    })

    $(function() {
        $(document).on("click", '.status', function(e) {
            var status = $(this).prop('checked') == true ? 1 : 0;
            var user_id = $(this).data('id');
            var data_status = $(this).attr('data-status');
            $.ajax({
                type: "GET",
                dataType: "json",
                url: '{{route("student.admission_confirm.changeStatus")}}',
                data: {
                    'status': status,
                    'user_id': user_id,
                    'data_status': data_status
                },
                success: function(res) {                  
                    var htm = "";
                    $('#messages').html();
                    htm = '<div class="message" style="color:red;text-align:center;font-size:18px">' + res.message + '</div>';
                    $('#messages').html(htm); 
                    $('#myform').hide();
                    $('#myform').val('');                   
                }
            });
        })
    })
</script>
@endpush