@extends('layouts.user.master')
@section('title','Profile')
@section('content')
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            College Profile
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Examples</a></li>
            <li class="active">College profile</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content ">
        <div class="row justify-content-center">
            <!-- /.col -->
            <div class="col-md-6 form-group col-md-offset-3 align-center">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li><a href="#settings" data-toggle="tab">Profile</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="settings">
                            <form class="form-horizontal" enctype="multipart/form-data" method="post" action="{{route('student.student.profile')}}" id="profileupdate">
                                @csrf
                                <!-- <div class="form-group" style="text-align:center">                                
                                <div class="col-sm-12" >
                                    <img src="{{$admin->logo}}" name="image" width="100px" height="100px"></img>
                                </div>
                                </div> -->

                                <div class="form-group" style="text-align:center">
                                    <div class="col-sm-12">
                                        <img src="{{$admin->image}}" id="upload-img" width="100px" height="100px"></img>
                                    </div>
                                    <label for="inputName" class="col-sm-2 control-label">Image</label>
                                    <div class="col-sm-10">
                                        <input type="file" class="form-control" name="image" name="name">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputName" class="col-sm-2 control-label">Name</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="name" value="{{$admin->name}}" id="inputName" placeholder="Name">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail" class="col-sm-2 control-label">Email</label>
                                    <div class="col-sm-10">
                                        <input type="email" class="form-control" value="{{$admin->email}}" readonly id="inputEmail" placeholder="Email">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputEmail" class="col-sm-2 control-label">Contact_no</label>
                                    <div class="col-sm-10">
                                        <input type="email" class="form-control" value="{{$admin->contact_no}}" readonly id="inputEmail" placeholder="Email">
                                    </div>
                                </div>
                               
                                <div class="form-group">
                                    <label for="inputEmail" class="col-sm-2 control-label">Address</label>
                                    <div class="col-sm-10">
                                        <input type="email" class="form-control" value="{{$admin->address}}" readonly id="inputEmail" placeholder="Email">
                                    </div>
                                </div>

                               
                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10">
                                        <button type="submit" id="update" class="btn btn-primary">Update</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
@endsection

@push('js')

@endpush