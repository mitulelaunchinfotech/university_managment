@extends('layouts.user.master')
@section('title','Reserved Quota')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Student Reserved Quota Confirm List
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Tables</a></li>
            <li class="active">Student</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="modal fade" id="bookModal">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">Update Book</h4>
                            </div>
                            <div class="modal-body">
                                <form id="editform" enctype="multipart/form-data">
                                    @csrf
                                    <div class="box-body">
                                        <div div class="form-group">
                                            <label>Category</label>
                                            <select class="form-control" name="category_id">
                                                <option>Select Category</option>
                                                <option value=""></option>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label>Title</label>
                                            <input type="text" class="form-control" name="title" id="title" placeholder="Enter title">
                                        </div>

                                        <div class="form-group">
                                            <label>Author</label>
                                            <input type="text" class="form-control" name="author" id="author" placeholder="Enter author">
                                        </div>

                                        <div class="form-group">
                                            <label>No_of_copy</label>
                                            <input type="text" class="form-control" name="no_of_copy" id="no_of_copy" placeholder="Enter No_of_copy">
                                        </div>

                                        <div div class="form-group">
                                            <label>Status</label>
                                            <select class="form-control" name="status" id="status">
                                                <option value="1">Available</option>
                                                <option value="2">Not Available</option>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label>Publish_year</label>
                                            <input type="text" class="form-control" name="publish" id="publish" placeholder="Enter category">
                                        </div>

                                        <div class="form-group">
                                            <label>Langauge</label>
                                            <input type="text" class="form-control" name="language" id="language" placeholder="Enter No_of_copy">
                                        </div>

                                        <div class="form-group">
                                            <label>Cover_image</label>
                                            <input type="file" class="form-control" name="cover_image" id="cover_image" placeholder="Enter category">
                                            <img src="" id="set_image" width="50px" height="50px"></img>
                                        </div>
                                    </div>
                                    <input type="hidden" id="id" name="id">
                                </form>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
                                    <button type="button" id="update" name="update" class="btn btn-primary">Update</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box">
                    <div class="box-body">                        
                        {!! $dataTable->table(['class' => 'table table-bordered dt-responsive nowrap']) !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="control-sidebar-bg"></div>
</div>
@endsection
@push('js')
{!! $dataTable->scripts() !!}
<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap4.min.js"></script>
@endpush