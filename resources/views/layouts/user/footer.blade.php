<footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.13
    </div>
    <strong>Copyright &copy; 2021-2022 <a href="https://adminlte.io/">University Managmnet System</a>.</strong> All rights
    reserved.
  </footer>