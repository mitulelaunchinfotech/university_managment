<header class="main-header">
  <!-- Logo -->
  <a href="index2.html" class="logo">
    <span class="logo-mini"><b>L</b>MS</span>
    <span class="logo-lg"><b>LMS</b></span>
  </a>
  <nav class="navbar navbar-static-top">
    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
      <span class="sr-only">Toggle navigation</span>
    </a>

    <div class="navbar-custom-menu">
      <ul class="nav navbar-nav">
        <li class="dropdown user user-menu">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <img src="{{ asset('asset/dist/img/user2-160x160.jpg') }}" class="user-image" alt="User Image">
            <span class="hidden-xs">{{ Auth::guard('collage')->user()->name }}</span>
          </a>
          <ul class="dropdown-menu">
            <!-- User image -->
            <li class="user-header">
              <img src="{{asset('asset/dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
              <p>
                Alexander Pierce - Web Developer
                <small>Member since Nov. 2012</small>
              </p>
            </li>
            <li class="user-footer">
              <div class="pull-right">
                <a href="{{route('collage.changepassword')}}" class="btn btn-default btn-flat">Chng Password</a>
                <a href="{{route('collage.admin.profile')}}" class="btn btn-default col-sm">Profile</a>
                <a>
                  <a href="{{ route('collage.logout') }}" class="btn btn-default col-sm" onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                    <i class="material-icons-outlined"></i>
                    {{ __('Logout') }}
                  </a>
                  <form id="logout-form" action="{{ route('collage.logout') }}" method="POST" class="d-none">
                    @csrf
                  </form>
                </a>
              </div>
            </li>
          </ul>
        </li>
        
      </ul>
    </div>
  </nav>
</header>