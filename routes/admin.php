<?php

use App\Http\Controllers\AdminController;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\University\AdmissionController;
use App\Http\Controllers\University\CollegeController;
use App\Http\Controllers\University\UniversityController;
use App\Http\Controllers\University\CommonSettingController;
use App\Http\Controllers\University\StudentController;
use App\Http\Controllers\University\CourseController;
use App\Http\Controllers\University\MeritRoundController;

Route::group(['namespace' => 'Auth'], function () {
    # Login Routes
    Route::get('login', 'LoginController@showLoginForm');
    Route::post('login', 'LoginController@login')->name('login');
    Route::post('admin/logout', 'LoginController@logout')->name('logout');
});

Route::group(['middleware' => 'auth:admin'], function () {
    Route::get('/dashboard', function () {
        return view('admin.dashboard');
    })->name('dashboard');

    Route::group(['prefix' => 'admin'], function () {
        Route::get('profile', [UniversityController::class, 'profile'])->name('admin.profile');
        Route::post('profile', [UniversityController::class, 'updateprofile']);
    });

    Route::group(['prefix'=>'college'],function(){
        Route::get('create',[CollegeController::class,'create'])->name('college.create');
        Route::get('index',[CollegeController::class,'index'])->name('college.index');
        Route::post('store',[CollegeController::class,'store'])->name('college.store');
        Route::get('edit/{id}',[CollegeController::class,'edit'])->name('college.edit');
        Route::post('update',[CollegeController::class,'update'])->name('college.update');
        Route::post('destroy',[CollegeController::class,'destroy'])->name('college.destroy');
    });   
        
    Route::group(['prefix'=>'common_setting','as'=>'common_setting.'],function(){
        Route::get('create',[CommonSettingController::class,'create'])->name('create');
        Route::get('index',[CommonSettingController::class,'index'])->name('index');
        Route::post('store',[CommonSettingController::class,'store'])->name('store');
        Route::get('edit/{id}',[CommonSettingController::class,'edit'])->name('edit');
        Route::post('update',[CommonSettingController::class,'update'])->name('update');
        Route::post('destroy',[CommonSettingController::class,'destroy'])->name('destroy');
    });  

    Route::group(['prefix'=>'student','as'=>'student.'],function(){
        Route::get('create',[StudentController::class,'create'])->name('create');        
        Route::get('index',[StudentController::class,'index'])->name('index');
        Route::post('destroy',[StudentController::class,'destroy'])->name('destroy');
    });
    
    
    Route::group(['prefix'=>'course','as'=>'course.'],function(){
        Route::get('create',[CourseController::class,'create'])->name('create');        
        Route::get('index',[CourseController::class,'index'])->name('index');
        Route::get('edit/{id}',[CourseController::class,'edit'])->name('edit');
        Route::post('update',[CourseController::class,'update'])->name('update');
        Route::post('destroy',[CourseController::class,'destroy'])->name('destroy');
    });


    Route::group(['prefix'=>'merit','as'=>'merit.'],function(){
        Route::get('create',[MeritRoundController::class,'create'])->name('create');        
        Route::get('index',[MeritRoundController::class,'index'])->name('index');
        Route::post('store',[MeritRoundController::class,'store'])->name('store');
        Route::get('edit/{id}',[MeritRoundController::class,'edit'])->name('edit');
        Route::post('update',[MeritRoundController::class,'update'])->name('update');
        Route::post('destroy',[MeritRoundController::class,'destroy'])->name('destroy');
    });


    Route::group(['prefix'=>'admission','as'=>'admission.'],function(){
        Route::get('create',[AdmissionController::class,'create'])->name('create');        
        Route::get('index',[AdmissionController::class,'index'])->name('index');
        Route::post('destroy',[AdmissionController::class,'destroy'])->name('destroy');
    });
});


?>
